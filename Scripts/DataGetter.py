# This script is used to import data from pro-football-reference.com

import csv
import requests
import numpy as np
from bs4 import BeautifulSoup
from StatStrings import StatStrings
from DataHelper import DataHelper
from datetime import datetime

class DataGetter:
    def __init__(self):
        self.url = "https://www.pro-football-reference.com"
        self.error_msg = ['', '']
        self.writeData = True
        self.teams = [
        'htx', 'clt', 'oti', 'jax',
        'nwe', 'mia', 'buf', 'nyj',
        'rav', 'pit', 'cle', 'cin',
        'kan', 'sdg', 'den', 'rai',
        'dal', 'phi', 'was', 'nyg',
        'chi', 'min', 'gnb', 'det',
        'nor', 'atl', 'car', 'tam',
        'ram', 'sea', 'sfo', 'crd'
        ]
        self.stats = StatStrings()

    def get_row(self, info, key, value, row):
        dat = row.find('td', attrs={'data-stat': value})
        try:
            name = dat.get_text()
            if key not in info:
                info.update({key: []})
            name = name.replace('%', '')
            info[key].append(name)
        except:
            if key not in info:
                info.update({key: []})
            info[key].append(0.0)
            pass

    def structure_data(self, data, info, normalize):
        dh = DataHelper()
        for key, values in info.items():
            # remove last item because it's the total
            if normalize:
                data.append(dh.normalizeData(values[:-1], key))
            else:
                data.append(values[:-1])


    def write_error(self, msg, error):
        if self.error_msg != [msg, error]:
            self.error_msg = [msg, error]
        with open('errors.csv', mode='a') as file:
            writter = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writter.writerow(self.error_msg)

    def get_individual_player_data_gamelog(self, year, player, normalize):
        firstLetter = player[0]
        parsed_table = []
        try:
            r = requests.get(self.url + '/players/' + firstLetter + '/' + player + '/gamelog/' + str(year))
            soup = BeautifulSoup(r.content, 'html.parser')
            parsed_table = soup.find_all('table')[0]
        except:
            self.write_error("Could not get player data for the year " + str(year), "MISSING TABLE")
            print("Could not get player data for the year " + str(year))
            return []

        info = {}

        if self.stats.get_gamelog_dict() == {}:
            print("stats haven't been set for individual game logs")
            return

        for i, row in enumerate(parsed_table.find_all('tr')[1:]):
            for key, value in self.stats.get_gamelog_dict().items():
                # skip tittles
                if i == 0:
                    continue
                self.get_row(info, key, value, row)

        data = []
        self.structure_data(data, info, normalize)

        return data

    def get_individual_player_data_fantasy(self, year, player, normalize):
        firstLetter = player[0]
        try:
            r = requests.get(self.url + '/players/' + firstLetter + '/' + player + '/fantasy/' + str(year))
            soup = BeautifulSoup(r.content, 'html.parser')
            parsed_table = soup.find_all('table')[0]
        except:
            #self.write_error("Could not get player data for the year " + str(year), "MISSING TABLE")
            print('returning nothing')
            return []

        info = {}
        if self.stats.get_fantasy_dict() == {}:
            print("stats haven't been set for individual fantasy points")
            return

        for i, row in enumerate(parsed_table.find_all('tr')[1:]):
            for key, value in self.stats.get_fantasy_dict().items():
                # skip tittles
                if i == 0 or i == 1:
                    continue
                self.get_row(info, key, value, row)

        data = []
        self.structure_data(data, info, normalize)

        return data

    def get_team_data(self, year, team, normalize):
        try:
            r = requests.get(self.url + '/teams/' + str(team) + '/' + str(year) + '.htm')
            soup = BeautifulSoup(r.content, 'html.parser')
            parsed_table = soup.find_all('table')[1]
        except:
            self.write_error("Could not get team data for " + team + " in the year " + str(year), "MISSING TABLE")
            return []

        info = {}
        if self.stats.get_team_dict() == {}:
            print("stats haven't been set for team dictionary")
            return

        for i, row in enumerate(parsed_table.find_all('tr')[2:]):
            for key, value in self.stats.get_team_dict().items():
                # skip tittles
                if i == 0 or i == 1:
                    continue
                self.get_row(info, key, value, row)

        data = []
        self.structure_data(data, info, normalize)

        return data

    def get_gamelog_stats_from_current_week_to_past_weeks(self, startYear, startWeek, numberOfWeeks, player, normalize, getInputs):
        if (getInputs):
            startYearData = self.get_individual_player_data_gamelog(startYear, player, normalize)
        else:
            startYearData = self.get_individual_player_data_fantasy(startYear, player, normalize)

        # trim off weeks that took place after the week we want to start with
        #if len(startYearData[0]) > startWeek:
            #print("trimed off some data for startweek of " + str(startWeek))
            #startYearData = startYearData[startWeek:]

        numberOfWeeksStillNeeded = numberOfWeeks - len(startYearData)
        # this probably means we're asking for a year of data that hasn't happened yet
        if len(startYearData) < 1:
            print("Did not get any data for this year, and this case isn't handled yet")
            return []

        weeksOfDataGotSoFar = len(startYearData[0])
        weeksLeftToGet = numberOfWeeks - weeksOfDataGotSoFar

        # switch cols and rows
        startYearData = [list(i) for i in zip(*startYearData)]

        year = startYear - 1
        ret = startYearData
        count = 1
        while(weeksOfDataGotSoFar < numberOfWeeks):
            if (getInputs):
                previousYearsData = self.get_individual_player_data_gamelog(year, player, normalize)
            else:
                previousYearsData = self.get_individual_player_data_fantasy(year, player, normalize)
            # switch cols and rows
            previousYearsData = [list(i) for i in zip(*previousYearsData)]

            weeksOfDataGotSoFar = weeksOfDataGotSoFar + len(previousYearsData)
            if weeksOfDataGotSoFar > numberOfWeeks:
                t = weeksOfDataGotSoFar

            previousYearsData = previousYearsData[-weeksLeftToGet:]
            weeksOfDataGotSoFar = weeksOfDataGotSoFar + weeksLeftToGet
            year = year - 1
            count = count + 1
            ret = ret + previousYearsData

        return ret

    def get_gamelog_stats_and_fantasy_points(self, year, startWeek, numberOfWeeks, player, normalize):
        inputs = self.get_gamelog_stats_from_current_week_to_past_weeks(numberOfWeeks, player, normalize)
        outputs = self.get_fantasy_points_from_current_week_to_past_weeks(numberOfWeeks, player, normalize)
        return [inputs, outputs]

    # old
    def get_input_outputs_for_player(self, year, player, normalize):
        data = self.get_individual_player_data_gamelog(year, player, normalize) + self.get_individual_player_data_fantasy(year, player, normalize)
        # switch cols and rows
        inputs = [list(i) for i in zip(*data)]
        #convert to float
        inputs = [[float(y) for y in x] for x in inputs]
        # pull out last value in each list
        outputs = [item[-1] for item in inputs]

        return [inputs, outputs]

# need to move these keys to get_team_dict
'''
def write_team_data(year, team):
    data = []

    # day of game
    data.append(get_team_data(year, team, 'game_day_of_week'))
    # date of game
    data.append(get_team_data(year, team, 'game_date'))
    # time of game
    data.append(get_team_data(year, team, 'game_time'))
    # W or L
    data.append(get_team_data(year, team, 'game_outcome'))
    # overtime?
    data.append(get_team_data(year, team, 'overtime'))
    # record after game
    data.append(get_team_data(year, team, 'team_record'))
    # away @, blank if home game
    data.append(get_team_data(year, team, 'game_location'))
    # team opponent
    data.append(get_team_data(year, team, 'opp'))
    # team score
    data.append(get_team_data(year, team, 'pts_off'))
    # opponent score
    data.append(get_team_data(year, team, 'pts_def'))
    # total 1st downs
    data.append(get_team_data(year, team, 'first_down_off'))
    # total 1st downs given up
    data.append(get_team_data(year, team, 'yards_off'))
    # passing yards
    data.append(get_team_data(year, team, 'pass_yds_off'))
    # rushing yards
    data.append(get_team_data(year, team, 'rush_yds_off'))
    # turnovers
    data.append(get_team_data(year, team, 'to_off'))
    # 1st downs given up
    data.append(get_team_data(year, team, 'first_down_def'))
    # yards given up
    data.append(get_team_data(year, team, 'yards_def'))
    # passing yards given up
    data.append(get_team_data(year, team, 'pass_yds_def'))
    # rushing yards given up
    data.append(get_team_data(year, team, 'rush_yds_def'))
    # takeaways
    data.append(get_team_data(year, team, 'to_def'))
    # expected offense points
    data.append(get_team_data(year, team, 'exp_pts_off'))
    # expected defense points
    data.append(get_team_data(year, team, 'exp_pts_def'))
    # expected special teams points
    data.append(get_team_data(year, team, 'exp_pts_st'))

    data = np.asarray(data)

    if writeData:
        with open(team + '_data_' + str(year) + '.csv', mode='w') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerows(data)
        print("Team " + team + " data written for " + str(year))

    return data

'''
