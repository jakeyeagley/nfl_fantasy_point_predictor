class StatStrings:
    def __init__(self):
        self.gamelog_dict = {
            #'pass_cmp_per_game'            : 'pass_cmp',
            #'pass_att_per_game'            : 'pass_att',
            'pass_yards_per_game'          : 'pass_yds',
            #'pass_cmp_perc_per_game'       : 'pass_cmp_perc',
            'pass_td_per_game'             : 'pass_td',
            'pass_int_per_game'            : 'pass_int',
            #'passer_rating_per_game'       : 'pass_rating',
            #'sacked_per_game'              : 'pass_sacked',
            #'rush_attempts_per_game'       : 'rush_att',
            'rush_yards_per_game'          : 'rush_yds',
            'rushing_td_per_game'          : 'rush_td',
            #'targets_per_game'             : 'targets',
            'rec_yards_per_game'           : 'rec_yds',
            'rec_touchdowns_per_game'      : 'rec_td',
            'fumbles_lost'                 : 'fumbles_lost'
            #'catch_perc_per_game'          : 'catch_pct',
            #'all_non_passing_touchdowns'   : 'all_td',
        }

        self.fantasy_dict = {
            #'offensive_snaps_per_game'      : 'offense',
            #'pct_offensive_snaps_per_game'  : 'off_pct',
            'fantasy_points_per_game'       : 'fantasy_points'
        }

        self.team = { }
        self.player_year = { }

    def get_gamelog_dict(self):
        return self.gamelog_dict
    def get_fantasy_dict(self):
        return self.fantasy_dict;
    def get_team_dict(self):
        return self.team;

    def pass_comp_per_game(self):
        return 'pass_cmp_per_game'

    def pass_att_per_game(self):
        return 'pass_att_per_game'

    def pass_yards_per_game(self):
        return 'pass_yards_per_game'

    def pass_comp_percent(self):
        return 'pass_cmp_perc_per_game'

    def pass_td_per_game(self):
        return 'pass_td_per_game'

    def pass_int_per_game(self):
        return 'pass_int_per_game'

    def passer_rating_per_game(self):
        return 'passer_rating_per_game'

    def sacked_per_game(self):
        return 'sacked_per_game'

    def rush_attempts_per_game(self):
        return 'rush_attempts_per_game'

    def rush_yards_per_game(self):
        return 'rush_yards_per_game'

    def rush_touchdowns_per_game(self):
        return 'rushing_td_per_game'

    def targets_per_game(self):
        return 'targets_per_game'

    def rec_yards_per_game(self):
        return 'rec_yards_per_game'

    def rec_td_per_game(self):
        return 'rec_touchdowns_per_game'

    def catch_percentage_per_game(self):
        return 'catch_perc_per_game'

    def fumbles_lost(self):
        return 'fumbles_lost'

    def non_throwing_touchdowns(self):
        return 'all_non_passing_touchdowns'

    def offensive_snaps_per_game(self):
        return 'offensive_snaps_per_game'

    def pct_offensive_snaps_per_game(self):
        return 'pct_offensive_snaps_per_game'

    def fantasy_point_per_game(self):
        return 'fantasy_points_per_game'
