import math
from keras.models import Sequential
from keras.layers import Dense
import numpy as np

# to use Keras Python 3.6 or lower is required (not higher)
# must install tensorflow 2.0 (pip install tensorflow==2.0)

class KerasNeuralNetwork:
    def defineModel(self):
        self.model = Sequential()
        self.model.add(Dense(32, input_dim=18, activation='relu'))
        self.model.add(Dense(64, activation='relu'))
        self.model.add(Dense(1, activation='sigmoid'))

    def compile(self):
        self.model.compile(loss='mean_squared_error', optimizer='adam')

    def fit(self, inputs, output):
        self.model.fit(inputs, output, epochs=5, batch_size=1)

    def evaluate(self, inputs, output):
        return self.model.evaluate(inputs, output)

    def predictions(self, input):
        return np.argmax(self.model.predict(input), axis=-1)
