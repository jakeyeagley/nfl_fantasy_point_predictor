import numpy as np
from StatStrings import StatStrings

# min and max values for data

class DataHelper:
    def __init__(self):
        stats = StatStrings()
        self.minMaxValuesForIndividualPlayerSeason = {
            stats.pass_comp_per_game()           : [0.0, 46.0],  # record is 45
            stats.pass_att_per_game()            : [0.0, 71.0],  # record is 70
            stats.pass_yards_per_game()          : [-10.0, 564.0],  # record is 554
            stats.pass_comp_percent()            : [0.0, 100.0],
            stats.pass_td_per_game()             : [0.0, 7.0],
            stats.pass_int_per_game()            : [0.0, 8.0],
            stats.passer_rating_per_game()       : [0.0, 158.3],
            stats.sacked_per_game()              : [0.0, 12.0],
            stats.rush_attempts_per_game()       : [0.0, 45],
            stats.rush_yards_per_game()          : [-10.0, 300],  # record is 296
            stats.rush_touchdowns_per_game()     : [0.0, 6.0],
            stats.targets_per_game()             : [0.0, 28.0],
            stats.rec_yards_per_game()           : [0.0, 340.0], # record is 336
            stats.rec_td_per_game()              : [0.0, 5.0],
            stats.catch_percentage_per_game()    : [0.0, 100.0],
            stats.non_throwing_touchdowns()      : [0.0, 6.0],
            stats.offensive_snaps_per_game()     : [0.0, 101.2], # not sure what the record here is
            stats.pct_offensive_snaps_per_game() : [0.0, 105.0], # sometimes this can be more than 100%?
            stats.fantasy_point_per_game()       : [-1.0, 60.0],  # I found that 58.3 might be the record
            stats.fumbles_lost()                 : [0.0, 3.0],  # guess
        }

    def normalize(self, x, min, max, key):
        n = x - min
        d = max - min
        value = round(n / d, 10)
        if value > 1 or value < 0:
            print('normalized value ' + key + ' of ' + str(value) + ' was outside of range 0-1.')
            print('x: ' + str(x))
            print('min: ' + str(min))
            print('max: ' + str(max))
            return 0
        return value

    def reverseNormalize(self, normalizedValue):
        stats = StatStrings()
        max = self.minMaxValuesForIndividualPlayerSeason[stats.fantasy_point_per_game()][1]
        min = self.minMaxValuesForIndividualPlayerSeason[stats.fantasy_point_per_game()][0]
        return normalizedValue * (max - min) + min

    def normalizeData(self, data, key):
        if key not in self.minMaxValuesForIndividualPlayerSeason or self.minMaxValuesForIndividualPlayerSeason[key] == []:
            print(key + ' has no min-max values set')
            return []

        normalizedData = []
        for d in data:
            normalizedData.append(self.normalize(float(d), self.minMaxValuesForIndividualPlayerSeason[key][0], self.minMaxValuesForIndividualPlayerSeason[key][1], key))
        return normalizedData

    def formatInputData(self, inputs):
        return np.array(inputs)

    def formatOutputData(self,outputs):
        target_output = np.array([outputs])
        return target_output.reshape(len(outputs),1)
