import numpy as np
import random
from datetime import date

class CustomNeuralNetwork:
    def __init__(self, useWeightsFromFile, bias, lr):
        self.useWeightsFromFile = useWeightsFromFile
        self.bias = bias
        self.lr = lr
        self.runName = ""

    def set_inputs(self, x):
        #if len(x) != len(self.weights):
            #print("ERROR: weights "+ str(len(self.weights)) +" and inputs "+ str(len(x)) +" do not match")
            #return
        self.inputs = x
        if self.useWeightsFromFile:
            self.weights = self.get_weights_from_file()
        else:
            self.weights = np.full((len(self.inputs[0]),1), random.random())

    def set_target_outputs(self, x):
        #if len(x[0]) != len(self.inputs):
            #print("ERROR: inputs "+ str(len(self.inputs)) +" and outputs "+ str(len(x[0])) +" do not match")
            #return
        self.target_outputs = x

    def get_error(self):
        return self.error_sum

    def feedForwardInputs(self):
        self.in_o = np.dot(self.inputs, self.weights) + self.bias

    def feedForwardOutputs(self):
        self.out_o = self.sigmoid(self.in_o)

    def backPropogation(self):
        self.error = self.out_o - self.target_outputs
        self.error_sum = abs(self.error.sum())

    def calculateDerivative(self):
        self.derror_douto = self.error
        self.douto_dino = self.sigmoid_derivative(self.out_o)

    def multiplyDerivatives(self):
        self.deriv = self.derror_douto * self.douto_dino
        self.deriv_final = np.dot(self.inputs.T, self.deriv)

    def updateWeights(self):
        self.weights -= self.lr * self.deriv_final

    def updateBias(self):
        for i in self.deriv:
            self.bias -= self.lr * i

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def sigmoid_derivative(self, x):
        f = 1 / (1 + np.exp(-x))
        return f * (1 - f)

    def learn(self, epoch, print_error):
        for e in range(epoch):
            #feeding forward inputs:
            self.feedForwardInputs();
            #feeding forward output:
            self.feedForwardOutputs();
            #backpropogation and calculating error
            self.backPropogation()
            #calculating derivative:
            self.calculateDerivative()
            #multiplying individual derivatives and multiplying with the 3rd individual derivative:
            self.multiplyDerivatives()
            #updating the weights values:
            self.updateWeights()
            #updating the bias weigth value:
            self.updateBias()
            #print error
            if print_error:
                print(self.error_sum)

    def predict(self, single_point):
        result = np.dot(single_point, self.weights) + self.bias
        return self.sigmoid(result)

    def save_weights(self):
        w = []
        if self.runName != "":
            w.append(self.runName + " ")

        today = date.today()
        w.append(today.strftime("%m/%d/%y"))
        w.append("\n")
        for layer in self.weights:
            for weight in layer:
                w.append(repr(weight) + "\n")
        s = ""
        s = ''.join(w)

        with open('weights.txt', 'w') as file:
            file.write(s)


    def get_weights_from_file(self):
        with open('weights.txt', 'r') as file:
            lines = file.readlines()[1:]
            weights = []
            for w in lines:
                w = w.strip('\n')
                weights.append([float(w)])

            return np.array(weights)
